import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Main {

    WebDriver driver;

    @BeforeClass
    public static void mainPrecondition(){
        System.setProperty("webdriver.gecko.driver", "C:\\Maven\\geckodriver-v0.26.0-win64\\geckodriver.exe");
    }

    @Before
    public void preCondition(){
        driver = new FirefoxDriver();
    }

    @Test
    public void  registrationNewUser () throws InterruptedException {
        driver.get("https://user-data.hillel.it/html/registration.html");
        driver.findElement(By.className("registration")).click();
        driver.findElement(By.id("first_name")).sendKeys("Alehandro");
        driver.findElement(By.id("last_name")).sendKeys("Alvares");
        driver.findElement(By.id("field_work_phone")).sendKeys("7778877");
        driver.findElement(By.id("field_phone")).sendKeys("380500251471");
        driver.findElement(By.id("field_email")).sendKeys("alehandros1@gmail.com");
        driver.findElement(By.id("field_password")).sendKeys("Aleha12345");
        driver.findElement(By.id("male")).click();
        driver.findElement(By.cssSelector("#position>option[value='qa']")).click();
        driver.findElement(By.id("button_account")).click();
        Thread.sleep(1000);
        driver.switchTo().alert().accept();
        driver.get("https://user-data.hillel.it/html/registration.html");
        driver.findElement(By.id("email")).sendKeys("alehandros1@gmail.com");
        driver.findElement(By.id("password")).sendKeys("Aleha12345");
        driver.findElement(By.className("login_button")).click();
        driver.findElement(By.cssSelector("#info")).click();
        Thread.sleep(2000);
    }

    @After
    public void after(){
        driver.quit();
    }
}
